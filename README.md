# Aphantasia survey report

Aphantasia is a recently discovered condition, which prevents affected people from building mental images, whether from memory or imagination. This repository contains the data and report for a small citizen science survey that was inspired from a well-known scientific questionnaire, designed to better assess the prevalence of _aphantasia_ in the general population.

### What am I looking for? ###

You most likely want to take a look at  [report.ipynb](report.ipynb), which you should be able to view [in a nice format here](http://nbviewer.ipython.org/urls/bitbucket.org/agilly/aphantasia/raw/master/report.ipynb). If you are unable to view it, please perform the following steps:

* Click on the following link: [report.ipynb](report.ipynb)
* Click on the "Raw" button in the top right corner
* Copy the address from your address bar
* Go to [NbViewer](http://nbviewer.ipython.org) and paste the address

### Contact ###

* For any question about this survey, please email ag15@sanger.ac.uk